package br.com.itau;

public class Elevador {

    private int andar;
    private int totalAndares;
    private int capacidade;
    private int pessoasAbordo;

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public int getTotalAndares() {
        return totalAndares;
    }

    public void setTotalAndares(int totalAndares) {
        this.totalAndares = totalAndares;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getPessoasAbordo() {
        return pessoasAbordo;
    }

    public void setPessoasAbordo(int pessoasAbordo) {
        this.pessoasAbordo = pessoasAbordo;
    }

    public Elevador(int totalAndares, int capacidade) {
        this.totalAndares = totalAndares;
        this.capacidade = capacidade;
        this.andar = 0;
    }

    public int entra() {
        if (pessoasAbordo < capacidade)
            pessoasAbordo++;

        return pessoasAbordo;
    }

    public int sai() {
        if (pessoasAbordo > 0)
            pessoasAbordo--;

        return pessoasAbordo;
    }

    public int sobe() {
        if (andar < totalAndares)
            andar++;

        return andar;
    }

    public int desce() {
        if (andar > 0)
            andar--;

        return andar;
    }

}
